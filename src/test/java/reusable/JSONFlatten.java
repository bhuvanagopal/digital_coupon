package reusable;

import com.github.wnameless.json.flattener.JsonFlattener;
import org.json.JSONArray;
import org.json.JSONObject;

public class JSONFlatten {
    public JSONObject flattenNestedObject(String jsonObject) {
        String completeFlattenArray = "{ \"data\" : [";
        JSONArray jsonArray = new JSONArray(jsonObject);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            if(i==jsonArray.length()-1) {
                String jsonStr = JsonFlattener.flatten(obj.toString());
                System.out.println("last jsonStr is "+jsonStr);
                completeFlattenArray+=jsonStr;
            }
            else {
                String jsonStr = JsonFlattener.flatten(obj.toString());
                System.out.println("jsonStr is " + jsonStr);
                completeFlattenArray += jsonStr + ",";
            }
        }
        completeFlattenArray+="]}";
        JSONObject flattenedObj = new JSONObject(completeFlattenArray);
        System.out.println("flattenedObj is "+flattenedObj.toString());

        return flattenedObj;
    }
}
