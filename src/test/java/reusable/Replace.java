package reusable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Iterator;

public class Replace {
    public JSONObject replaceStr(String response) {
        String value = response.replace("T00:00:00.000Z", "T00:00:00Z");
        JSONObject jsonObj = new JSONObject(value);
        return jsonObj;
    }
}
