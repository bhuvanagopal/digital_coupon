package reusable;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.sql.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class InMemDb {
	static Statement stmt;
	static Connection conn;

	static {
		conn = getConnection();
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	static void createTable(JSONArray jsonArray, String tableName) throws SQLException {
		// iterate through the json objects to create a list of insert queries
		List<String> insertList = new ArrayList<String>();
		HashMap<String, String> fieldTypes = new HashMap<String, String>();
		Iterator<JSONObject> iterator = jsonArray.iterator();
		while (iterator.hasNext()) {
			String insertSql = "insert into " + tableName + "";
			String keys = "(";
			String vals = "(";
			JSONObject element = iterator.next();
			Set<Entry<String, Object>> entrySet = element.entrySet();
			for (Entry<String, Object> s : entrySet) {
				keys = keys + s.getKey() + ",";
				if(s.getValue()==null)
				{
					vals = vals  + "null,";
				}
				else {
					vals = vals + "'" + s.getValue() + "',";
				}
				// create hashmap columnName:columnType
				if (!fieldTypes.containsKey(s.getKey()))
					fieldTypes.put(s.getKey(), getColumnType(s));
			}

			insertSql = insertSql + keys.substring(0, keys.length() - 1) + ") values"
					+ vals.substring(0, vals.length() - 1) + ");";
			insertList.add(insertSql);
		}
		String createQuery = "create table " + tableName + "(" + fieldTypes.entrySet().stream()
				.map(entry -> entry.getKey() + " " + entry.getValue()).collect(Collectors.joining(",")) + ");";
		insertList.add(0, createQuery);
		// insertList.add(0, aggregateQuery);
		executeQuery(insertList);
		// create table
		// group insert
	}

	static void executeQuery(List<String> queryList) throws SQLException {
		String createQuery = queryList.get(0);
		queryList.remove(0);
		// queryList.remove(0);
		System.out.println(createQuery);
		stmt.execute(createQuery);
		for (String q : queryList) {
			// System.out.println(q);
			stmt.execute(q);
		}
	}

	static String executeQuery(String query, String tableName) throws SQLException {
		PreparedStatement preparedStatement = null;
		preparedStatement = conn.prepareStatement(query);
		ResultSet rs = preparedStatement.executeQuery();
		String aggResult = "";
		aggResult = convert(rs);
		preparedStatement.close();
		String dropQuery = "drop table " + tableName + ";";
		Statement statement = conn.createStatement();
		statement.execute(dropQuery);
		return aggResult;
	}

	static String executeQueryWithoutClearingTable(String query) throws SQLException {
		PreparedStatement preparedStatement = null;
		preparedStatement = conn.prepareStatement(query);
		ResultSet rs = preparedStatement.executeQuery();
		String aggResult = "";
		aggResult = convert(rs);
		preparedStatement.close();
		return aggResult;
	}

	static String executeAssertionQuery(String query, String tableName1, String tableName2) throws SQLException {
		PreparedStatement preparedStatement = null;
		preparedStatement = conn.prepareStatement(query);
		ResultSet rs = preparedStatement.executeQuery();
		String aggResult = "";
		aggResult = convert(rs);
		preparedStatement.close();
		String dropQuery = "drop table " + tableName1 + ";";
		Statement statement = conn.createStatement();
		statement.execute(dropQuery);
		dropQuery = "drop table " + tableName2 + ";";
		statement.execute(dropQuery);
		return aggResult;
	}

	private static String getColumnType(Entry<String, Object> s) {

		if (s.getValue() instanceof Number) {
			if (s.getValue() instanceof Double)
				return "double";
			else
				return "bigint";
		}
		if (s.getValue() instanceof String) {
			return "text";
		}
		return "text";

	}

	@SuppressWarnings("unchecked")
//	public static String convert(ResultSet rs) throws SQLException {
//		JsonObject jsonResponse = new JsonObject();
//		JsonArray data = new JsonArray();
//		ResultSetMetaData rsmd = rs.getMetaData();
//		while (rs.next()) {
//			int numColumns = rsmd.getColumnCount();
//			JsonObject row = null;
//			row = new JsonObject();
//			for (int i = 1; i < numColumns + 1; i++) {
//				String column_name = rsmd.getColumnName(i);
//				if (String.valueOf(rs.getString(column_name)).matches("-?\\d+(\\.\\d+)?"))
//					row.add(column_name, new JsonPrimitive(rs.getInt(column_name)));
//				else
//					row.add(column_name, new JsonPrimitive(String.valueOf(rs.getString(column_name))));
//
//
//
//			}
//			data.add(row);
//		}
//		jsonResponse.add("result", data);
//		String res = jsonResponse.toString();
//		return res;
//	}
	public static String convert(ResultSet rs) throws SQLException {
		JsonObject jsonResponse = new JsonObject();
		JsonArray data = new JsonArray();
		ResultSetMetaData rsmd = rs.getMetaData();
		while (rs.next()) {
			int numColumns = rsmd.getColumnCount();
			JsonObject row = null;
			row = new JsonObject();
			for (int i = 1; i < numColumns + 1; i++) {
				String column_name = rsmd.getColumnName(i);
				if (String.valueOf(rs.getString(column_name)).matches("\\d+$/?"))
					row.add(column_name, new JsonPrimitive(rs.getInt(column_name)));
				else if (String.valueOf(rs.getString(column_name)).matches("-?\\d+(\\.\\d+)?"))
					row.add(column_name, new JsonPrimitive(rs.getDouble(column_name)));
				else
					row.add(column_name, new JsonPrimitive(String.valueOf(rs.getString(column_name))));



			}
			data.add(row);
		}
		jsonResponse.add("result", data);
		String res = jsonResponse.toString();
		return res;
	}

	private static Connection getConnection() {
		try {
			Class.forName("org.sqlite.JDBC");
			return DriverManager.getConnection("jdbc:sqlite::memory:");
		} catch (final Exception e) {
			System.out.println("Failed to get connection." + e.getMessage());
			throw new RuntimeException(e);
		}
	}
}
