package reusable;

import com.github.wnameless.json.flattener.JsonFlattener;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;

public class JsonToTableWithSolr {
    static String tableName;

    public JsonToTableWithSolr(String tableName) {
        JsonToTableWithSolr.tableName = tableName;
    }

    public String callTable(String args1, String args2) {
    	System.out.println("Table name given is: "+tableName);
        try {
            getJsonCall(tableName, args1);
            String result = InMemDb.executeQuery(args2,tableName);
//            InMemDb.dropTable(tableName);
            System.out.println("SolrResult : "+result);
            return result;
        } catch (IOException | ParseException | SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return "operation unsuccessful";
    }

    public String callTableSolr(String args1, String args2) {
        System.out.println("Table name given is: "+tableName);
        try {
            getJsonCall(tableName, args1);
            String result = InMemDb.executeQueryWithoutClearingTable(args2);
//            InMemDb.dropTable(tableName);
            System.out.println("SolrResult : "+result);
            return result;
        } catch (IOException | ParseException | SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return "operation unsuccessful";
    }

    public String callTableGQL(String response, String aggGQLquery, String tabName) {
        System.out.println("Table name given is: "+tableName);
        try {
            getJsonCallGQl(tableName, response, tabName);
            String result = InMemDb.executeQueryWithoutClearingTable(aggGQLquery);
//            InMemDb.dropTable(tableName);
            System.out.println("GQLresult : "+result);
            return result;
        } catch (IOException | ParseException | SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return "operation unsuccessful";
    }

    public String assertionQuery(String assertionQuery,String table1,String table2)
    {
        System.out.println("QUERY : "+assertionQuery);
        try {
        String result = InMemDb.executeAssertionQuery(assertionQuery,table1,table2);
        System.out.println("AssertionResult : "+result);
        return result;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return "operation unsuccessful";
    }

    static String getJsonCall(String tableName, String solrQuery) throws IOException, ParseException, SQLException {
        URL obj = new URL(solrQuery);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        BufferedReader in;
        if (responseCode == HttpURLConnection.HTTP_OK) {
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            JSONParser parser = new JSONParser();
            JSONObject jObj = (JSONObject) parser.parse(response.toString());
            jObj = (JSONObject) jObj.get("response");
            JSONArray arr = (JSONArray) jObj.get("docs");
            InMemDb.createTable(arr, tableName);
            return response.toString();
        } else
            in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
        return "";

    }

    static String getJsonCallGQl(String tableName, String response, String tabName) throws IOException, ParseException, SQLException {
            JSONParser parser = new JSONParser();
            JSONObject jObj = (JSONObject) parser.parse(response);
            jObj = (JSONObject) jObj.get("data");
            JSONArray arr;
            if(tabName.equalsIgnoreCase("brandVolumeSourcing"))
            {
                JSONObject jObj1 = (JSONObject) jObj.get("brandVolumeSourcing");
                 arr = (JSONArray) jObj1.get("brands");
            }
            else if(tabName.equalsIgnoreCase("campaignResponderTrend") || tabName.equalsIgnoreCase("campaignIncrementality"))
            {
                JSONObject jObj1 = (JSONObject) jObj.get("campaign");
                arr = (JSONArray) jObj1.get(tabName);
            }
            else if(tabName.equalsIgnoreCase("buyerMigrations"))
            {
                parser = new JSONParser();
                Object obj  = parser.parse(jObj.get("buyerMigrations").toString());
                JSONArray array = new JSONArray();
                array.add(obj);
                arr= array;
            }
            else if(tabName.equalsIgnoreCase("campaignBuyersResponder"))
            {
                JSONObject jObj1 = (JSONObject) jObj.get("campaign");
                JSONArray arr1 = (JSONArray) jObj1.get("campaignBuyersResponder");
                JSONObject jObj2 = (JSONObject) arr1.get(0);
                JSONArray arr2 = (JSONArray) jObj2.get("topBrands");

                String completeFlattenArray = "{ \"data\": [{";
                org.json.JSONArray jsonArray = new org.json.JSONArray(arr2);
                for (int i = 0; i < jsonArray.length(); i++) {
                    org.json.JSONObject obj = jsonArray.getJSONObject(i);
                    if(i==jsonArray.length()-1) {
                        String jsonStr = JsonFlattener.flatten(obj.toString());
                        completeFlattenArray+=jsonStr.replace("label","label"+i).replace("value","value"+i)
                                .replace("{","").replace("}","");
                    }
                    else {
                        String jsonStr = JsonFlattener.flatten(obj.toString());
                        completeFlattenArray += jsonStr.replace("label","label"+i).replace("value","value"+i)
                                .replace("{","").replace("}","") + ",";
                    }
                }
                completeFlattenArray+="}]}";
                System.out.println("completeFlattenArray is "+completeFlattenArray);
                JSONParser parser1 = new JSONParser();
                JSONObject json = (JSONObject) parser1.parse(completeFlattenArray);
                arr = (JSONArray) json.get("data");
            }
            else {
                arr = (JSONArray) jObj.get(tabName);
            }
            InMemDb.createTable(arr, tableName);
            return response;
    }

    static String getColumnsCall() throws IOException {
        URL obj = new URL("");
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        System.out.println("GET Response Code :: " + responseCode);
        BufferedReader in;
        if (responseCode == HttpURLConnection.HTTP_BAD_REQUEST)
            in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
        else
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        System.out.println(response.toString());
        return response.toString();
    }

}
