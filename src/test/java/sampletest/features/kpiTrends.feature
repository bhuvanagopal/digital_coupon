@kpiTrends @brand @hub360
Feature: karate api test script for validating KPI Trends chart in BrandsPage

* configure headers = headers

  Background:
    * def fn = read('classpath:sampletest/features/utils/sendTCResultToTP.js');
    * def afterScenarioCall = read("../hooks/afterScenario.js")
    * configure afterScenario =
            """
	            function() {
	                afterScenarioCall(karate.info);
	                var info = karate.info;
                    karate.log('after', info.scenarioType + ':', info.scenarioName);
                    fn(info.scenarioName, info.errorMessage);
	            }
            """
    * def afterFeatureCall = read("../hooks/afterFeature.js")
    * configure afterFeature =
            """
            function() {
                afterFeatureCall();
            }
            """
    Given url baseUrl.GQLEndpoint
    * def payload =  read('../dataFiles/payload.json')
    * def responseFromTokenGenerator = call read('sessionTokenGeneration.feature')
    And def solrQuery = read('../dataFiles/solrQueries.json')

  Scenario Outline: Comparing response for attribution tab from GQL and Solr endpoint TestcaseIds 133819
    Given url baseUrl.GQLEndpoint
    * header Authorization = "Bearer "+ responseFromTokenGenerator.retrievedToken
    * def switchClient = payload.switchClient
    * set switchClient.variables.groupId = <groupId>
    When request switchClient
    And method post
    Then status 200
    * header Authorization = "Bearer "+ responseFromTokenGenerator.retrievedToken
    * def kpiTrend = payload.kpiTrendPayload
    * set kpiTrend.variables.listId = <listId>
    * set kpiTrend.variables.timeframe = <timeframe>
    When request kpiTrend
    And method post
    Then status 200
    And def gqlResponse = response
    And print "GQL Response is:", gqlResponse
    * def updateValue =
      """
        function(gqlResponse) {
          var callReplace = Java.type('reusable.Replace')
          replaceObj = new callReplace();
          return replaceObj.replaceStr(gqlResponse);
        }
      """
    * def updatedGQL = call updateValue gqlResponse
    And print "updated gql response is ",updatedGQL
    And def queryParameter = solrQuery.kpiTrendBrandQuery
    And set queryParameter.solrQuery = baseUrl.SolrEndpoint+queryParameter.solrQuery
    * def reusableMethod =
      """
        function(queryParameter) {
            var solrQueryObj = queryParameter;
            var aggGQLQuery = solrQueryObj.aggGQLQuery;
            var solrQuery = solrQueryObj.solrQuery.replace("%listNumber%", <listId>).replace("%timeFrame%", <timeframe>);
            var aggQuery = solrQueryObj.aggQuery;
            var assertionQuery = aggGQLQuery+" EXCEPT "+aggQuery;
            var result;
            var JavaDemo = Java.type('reusable.JsonToTableWithSolr')
            ob = new JavaDemo("gqlTable");
            ob.callTableGQL(updatedGQL, aggGQLQuery, "brandKpiTrend");
            ob = new JavaDemo("item2");
            ob.callTableSolr(solrQuery, aggQuery);
            result = ob.assertionQuery(assertionQuery,"gqlTable","item2");
            return result;
          }
      """
    * def resultFromInMemDB = call reusableMethod queryParameter
    And print "resultFromInMemDB is :",resultFromInMemDB
    * def comparison =
          """
            function(resultFromInMemDB) {
              var calldbComparison = Java.type('reusable.DbComparision')
              dbComparisonObj = new calldbComparison();
              return dbComparisonObj.compareDb(resultFromInMemDB);
            }
          """
    * def getComparisonValue = call comparison resultFromInMemDB

    Examples:
      | groupId | listId  | timeframe |
      |   441   | 1737880 | 26        |

