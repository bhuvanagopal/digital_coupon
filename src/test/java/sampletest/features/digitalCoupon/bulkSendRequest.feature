Feature: Bulk Send

  Scenario:  Bulk Send Request

    # get BL number and token
    * def value = {blNumber: '#(blNumber)', accessToken: '#(accessToken)'}

    # read payload and set the BL number
    * def payload = read('../../dataFiles/payload.json')
    * set payload.bulkSendRequestPayload[0].media_id = value.blNumber
    * print "Payload : ", payload.bulkSendRequestPayload

    # Get Redemption Request
    * def bulkSendEndpoint = "https://devpartner.api.catalina.com/hub/rest/media/event/barcode/bulk"
    Given url bulkSendEndpoint
    * header Authorization = value.accessToken
    And request payload.bulkSendRequestPayload
    When method POST
    Then status 200
    Then print "Response from get redemption request :",response
