@getAccessToken

  Feature: Get Access Token

    Scenario:  Generate access token based on retailer

      # read payload
      * def payload = read('../../dataFiles/payload.json')
      * print "Payload : ", payload.accessTokenPayload

      # get access token request
      * def getTokenEndpoint = "https://devpartner.api.catalina.com/security/auth/token"
      Given url getTokenEndpoint
      And request payload.accessTokenPayload[0]
      When method POST
      Then status 200
      Then print "Response containing access token :",response
