@getMedia

Feature: Get Media

  Background:
    # Target process integration code
    * def fn = read('classpath:sampletest/features/utils/sendTCResultToTP.js');
    * def afterScenarioCall = read("../../hooks/afterScenario.js")
    * configure afterScenario =
            """
	            function() {
	                afterScenarioCall(karate.info);
	                var info = karate.info;
                    karate.log('after', info.scenarioType + ':', info.scenarioName);
                    fn(info.scenarioName, info.errorMessage);
	            }
            """
    * def afterFeatureCall = read("../../hooks/afterFeature.js")
    * configure afterFeature =
            """
            function() {
                afterFeatureCall();
            }
            """

  Scenario Outline:  Get media for a given retailer TestcaseIds 198235,199671,198256,198319,198923

    # get Access Token
    * def accessTokenResponse = call read('getAccessToken.feature')
    * def accessToken = accessTokenResponse.response.token
    * print "Token : ",accessToken

    # Get Media Request
    * def getMediaEndpoint = "https://devpartner.api.catalina.com/hub/rest/media"
    Given url getMediaEndpoint
    * header Authorization = accessToken
    * param network_id = "<networkId>"
    * param stop_date = '[NOW/DAY TO *]'
    When method GET
    Then status 200
    Then print "Response from get media request :",response
    * def getMediaResponse = response

    # read payload for few expected Bl numbers
    * def payload = read('../../dataFiles/payload.json')
    * print "Expected BL numbers to be present in response : ", payload.expectedBLNumbersForDaiei

    # validate response with expected bl numbers and format for retailer
    Then match getMediaResponse.docs[*].format contains "digital"
    Then match getMediaResponse.docs[*].id contains payload.expectedBLNumbersForDaiei

    # validate response with expected bl numbers and format for other retailer which should not be present
    * print "Expected BL numbers for different retailer which should not to be present in response : ", payload.expectedBLNumbersForLine
    Then match getMediaResponse.docs[*].format !contains "line"
    Then match getMediaResponse.docs[*].id !contains payload.expectedBLNumbersForLine

    # get all BL numbers in a json array with token
    * def getBLdetails =
    """
      function (getMediaResponse, accessToken) {
        var couponArray = [];
        for (var i = 0; i < getMediaResponse.docs.length ; i++){
          couponArray.push({
             blNumber : getMediaResponse.docs[i].id,
             accessToken : accessToken
          })
        }
        return couponArray;
      }
    """
    * def couponJson = getBLdetails(getMediaResponse, accessToken)
    * print "Coupon Json with access token : ",couponJson

    # Call Get redemption request for each coupon/Bl number
    * def redemptionResponse = call read('getRedemption.feature') couponJson

    # Call Bulk_Send request for each coupon/Bl number
    * def bulkSendResponse = call read('bulkSendRequest.feature') couponJson

    Examples:
      | networkId |
      | 31        |

  Scenario: Import Data Request TestcaseIds 198954

    # get Access Token
    * def accessTokenResponse = call read('getAccessToken.feature')
    * def accessToken = accessTokenResponse.response.token
    * print "Token : ",accessToken

    # Import data Request
    * def importDataEndpoint = "https://devpartner.api.catalina.com/hub/rest/media/11111/import"
    Given url importDataEndpoint
    * header Authorization = accessToken
    And multipart file file = { read:'C:\\Users\\csauto4\\Desktop\\DigitalCouponFramework\\karate_base_framework\\src\\test\\java\\sampletest\\dataFiles\\Import_111276.csv', filename: 'Import_111276.csv' }
    When method POST
    Then status 200
    Then print "Response from get redemption request :",response

  Scenario: Import Data Request with invalid csv filename

    # get Access Token
    * def accessTokenResponse = call read('getAccessToken.feature')
    * def accessToken = accessTokenResponse.response.token
    * print "Token : ",accessToken

    # Import data Request
    * def importDataEndpoint = "https://devpartner.api.catalina.com/hub/rest/media/11111/import"
    Given url importDataEndpoint
    * header Authorization = accessToken
    And multipart file file = { read:'C:\\Users\\csauto4\\Desktop\\DigitalCouponFramework\\karate_base_framework\\src\\test\\java\\sampletest\\dataFiles\\111276-dummyfile.csv', filename: '111276-dummyfile.csv' }
    When method POST
    Then status 400
    Then print "Response from get redemption request :",response
