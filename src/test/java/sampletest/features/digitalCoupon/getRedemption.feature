Feature: Get Redemption

  Scenario:  Get redemption for each of the BL numbers

    # get BL number and token
    * def value = {blNumber: '#(blNumber)', accessToken: '#(accessToken)'}

    # Get Redemption Request
    * def getRedemptionEndpoint = "https://devpartner.api.catalina.com/hub/rest/media/" + value.blNumber +"/availability"
    Given url getRedemptionEndpoint
    * header Authorization = value.accessToken
    When method GET
#    Then status 200
    Then print "Response from get redemption request :",response

    # validate response
#    * assert response.available == true