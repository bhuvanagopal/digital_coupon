package sampletest.features.utils;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;


import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class createBuildinTP {

    private final String MASTER_TESTPLAN_ID;
    private final String ACCESS_TOKEN;
    private final String HOST_NAME;
    private final String BUILD_NAME;

    public createBuildinTP() {
        this.ACCESS_TOKEN = getProperties("tp_AccessToken");
        this.MASTER_TESTPLAN_ID = getProperties("tp_MasterTestPlanID");
        this.HOST_NAME = getProperties("tp_HostName");

        if (System.getProperty("BuildName") == null) {
            this.BUILD_NAME = getProperties("tp_ApplicationBuildName");
        } else {
            this.BUILD_NAME = System.getProperty("BuildName");
        }
    }

    // Get properties from the targetProcess.properties file
    public String getProperties(String key) {
        sampletest.features.utils.ReadPropertiesFile readProp = new sampletest.features.utils.ReadPropertiesFile();
        Properties prop = null;
        try {
            prop = readProp.readPropertiesFile("./src/test/java/sampletest/features/utils/targetProcess.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Value for key " + key + " :" + prop.getProperty(key));
        return prop.getProperty(key);
    }

    //To Create the build in TP
    public String postBuildName() {
        String stringUrl = HOST_NAME + "/TestPlans/" + MASTER_TESTPLAN_ID + "/TestPlanRuns?" + "access_token=" + ACCESS_TOKEN;

        String testPlan = "[{\"name\":\"" + BUILD_NAME + "\"}]";

        return post(stringUrl, testPlan);
    }

    //To get the build details
    public String getBuildDetails(String testPlanID) throws IOException {
        String stringUrl = HOST_NAME + "/TestPlans/" + testPlanID + "/TestPlanRuns?access_token=" + ACCESS_TOKEN + "&where=" + URLEncoder.encode("(name eq '" + getBuildNameToday() + "')", "UTF-8");
        String resp = get(stringUrl);
        return resp;
    }

    private String getBuildNameToday() {
        String pattern = "MM_dd_yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        return BUILD_NAME;
    }

    //To Create the build if there is no build present
    public String createBuild() throws IOException {
        String resp = getBuildDetails(MASTER_TESTPLAN_ID);
        Configuration conf = Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS);
        String runName = JsonPath.using(conf).parse(resp).read("$.Items[0].Name");

        if (runName != null && runName.contains(getBuildNameToday())) {
            System.out.println("Build found");
            return resp;
        } else {
            System.out.println("Build not found. Creating new build with name " + getBuildNameToday());
            return postBuildName();
        }
    }

    //For getting test case run id
    public String getTCRunID(String TC_ID, String testPlanID) throws IOException {
        String resp = getBuildDetails(testPlanID);
        int build_id = JsonPath.parse(resp).read("$.Items[0].Id");

        System.out.print("getTCRunID method started");

        String conditions = URLEncoder.encode("(TestCase.ID eq " + TC_ID + ") and(RootTestPlanRun.ID eq " + build_id + ")", "UTF-8");
        String url = HOST_NAME + "/TestCaseRuns/?access_token=" + ACCESS_TOKEN + "&where=" + conditions;
        int testCaseId = JsonPath.parse(get(url)).read("$.Items[0].Id");

        System.out.print("Test case run id::" + testCaseId);
        return String.valueOf(testCaseId);
    }

    //To update test case result
    public void updateStatus(List<Integer> TC_Ids, String status) throws IOException {
        System.out.println("Check for TC ID "+TC_Ids+" and status "+status);
        String resp_getBuildId = createBuild();


        //if build exists, get the build_id
        int build_id = JsonPath.parse(resp_getBuildId).read("$.Items[0].Id");
        String TC_Id = getTC_Id_toUpdate(TC_Ids, String.valueOf(build_id));

        String testcaseRun_id = "";
        if(!TC_Id.isEmpty()) {
            testcaseRun_id = String.valueOf(getTCRunID(TC_Id, MASTER_TESTPLAN_ID));
        }
        System.out.print("\nUpdate status method started");

        String url = HOST_NAME + "/TestCaseRuns/" + testcaseRun_id + "?access_token=" + ACCESS_TOKEN;
        System.out.print("\nURL :" + url);

        String body = "{\"status\": \"" + status + "\"}";
        System.out.print("\nStatus body::" + body);

        post(url, body);
    }

    //To get test case status
    public String getTC_Id_toUpdate(List<Integer> TC_Id, String build_id) throws IOException {
        String condition1 = URLEncoder.encode("(RootTestPlanRun.ID eq " + build_id + ")", "UTF-8");
        String url = HOST_NAME + "/TestCaseRuns/?access_token=" + ACCESS_TOKEN + "&where=" + condition1 + "&include=[TestCase,Executed]&take=500";
        System.out.print("\nGet Status URL :" + url);

        String resp = get(url);
        String TC_Id_toUpdate = "";

        //get first test case that has Execution status false
        for(int i=0; i<TC_Id.size();i++){
//            if((JsonPath.parse(resp).read("$.Items[?(@.TestCase.Id=='"+ TC_Id.get(i) +"')].Executed")).toString().contains("false")) {
                System.out.print("\nTC ID TO UPDATE :" + TC_Id.get(i));
                TC_Id_toUpdate = String.valueOf(TC_Id.get(i));
//                break;
//            }
        }
        return TC_Id_toUpdate;
    }

    private String post(String stringUrl, String body) {
        HttpURLConnection connection = null;
        StringBuilder response = null;
        try {
            //Create connection
            URL url = new URL(stringUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setRequestProperty("User-Agent", "Java client");
            connection.setUseCaches(false);
            connection.setDoOutput(true);

            try (OutputStream os = connection.getOutputStream()) {
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            //Post response
//            int code = connection.getResponseCode();
//            System.out.println("STATUS CODE :" + code);
//            String resp = connection.getResponseMessage();
//            System.out.println("RESPONSE :" + resp);

            response = new StringBuilder();

            try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {

                String line;

                while ((line = br.readLine()) != null) {
                    response.append(line);
                    response.append(System.lineSeparator());
                }
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }

            System.out.println(response.toString());

        } catch (IOException unsupportedEncodingException) {
            unsupportedEncodingException.printStackTrace();
        } finally {

            assert connection != null;
            connection.disconnect();
        }

        return response.toString();
    }

    private String get(String stringUrl) throws IOException {
        URL url = new URL(stringUrl);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");

        //Post response
        int code = connection.getResponseCode();
//        System.out.print("\nSTATUS CODE : " + code);
//        String resp = connection.getResponseMessage();
//        System.out.print("\nRESPONSE : " + resp + "\n");

        StringBuilder response = new StringBuilder();

        if (code == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String inputLine;


            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

        } else {
            System.out.println("GET request not worked");
        }
        return response.toString();
    }
}

